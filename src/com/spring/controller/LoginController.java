package com.spring.controller;


import com.spring.util.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
@RequestMapping("/user")
public class LoginController {

    @RequestMapping("/login")
    public ModelAndView showForm(){
        ModelAndView form = new ModelAndView("login");
        return form;
    }

//    @RequestMapping(value = "/submit",method = RequestMethod.POST)
//    public ModelAndView processForm(@RequestParam("userName") String userName,@RequestParam("password") String password){
//        ModelAndView modelAndView;
//        if(userName.equals("admin") && password.equals("admin")) {
//            modelAndView = new ModelAndView("dashboard");
//            modelAndView.addObject("message","You Successfully Logged In");
//        }else{
//           modelAndView = new ModelAndView("error");
//           modelAndView.addObject("message","Invalid UserName/Password");
//        }
//        return modelAndView;
//    }

//    @RequestMapping(value = "/submit",method = RequestMethod.POST)
//    public ModelAndView processForm(@RequestParam Map<String,String> reqParam){
//        String userName = reqParam.get("userName");
//        String password = reqParam.get("password");
//        ModelAndView modelAndView;
//        if(userName.equals("admin") && password.equals("admin")) {
//            modelAndView = new ModelAndView("dashboard");
//            modelAndView.addObject("message","You Successfully Logged In");
//        }else{
//            modelAndView = new ModelAndView("error");
//            modelAndView.addObject("message","Invalid UserName/Password");
//        }
//        return modelAndView;
//    }

    @RequestMapping(value = "/submit",method = RequestMethod.POST)
    public ModelAndView processForm(@ModelAttribute("user") User user){

        ModelAndView modelAndView;
        if(user.userName.equals("admin") && user.password.equals("admin")) {
            modelAndView = new ModelAndView("dashboard");
            modelAndView.addObject("message","You Successfully Logged In");
        }else{
            modelAndView = new ModelAndView("error");
            modelAndView.addObject("message","Invalid UserName/Password");
        }
        return modelAndView;
    }

    @RequestMapping("/logout")
    public ModelAndView logout(){
        ModelAndView form = new ModelAndView("login");
        return form;
    }
}
